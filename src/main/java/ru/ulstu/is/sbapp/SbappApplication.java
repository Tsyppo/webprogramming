package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;

@SpringBootApplication
@RestController
public class SbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbappApplication.class, args);
	}

	@GetMapping("/hello")
    public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
        return String.format("Hello %s!", name);
	}

	//1 лаба: Добавить символов в конец существующей строки, в зависимости он цыфры в конце строки
	//2 лаба: Несколько реализаций 1 лабы в интерфейсе
	@GetMapping("/test")
	public String test(@RequestParam(value = "str")String str){
		String str2 = str.substring(str.length() - 1);
		int integer = Integer.parseInt(str2);
		Random rnd = new Random();
		StringBuilder strBuilder = new StringBuilder(str);
		for(int i = 0; i < integer; i++){
			strBuilder.append((char)('a' + rnd.nextInt(26)));
		}
		str = strBuilder.toString();
		return String.format("Result: %s", str);
	}

}
